package prog.kiev.ua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import prog.kiev.ua.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
