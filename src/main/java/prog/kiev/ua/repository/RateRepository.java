package prog.kiev.ua.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import prog.kiev.ua.entity.Currency;
import prog.kiev.ua.entity.ExchangeRate;

import java.util.Optional;

@Repository
public interface RateRepository extends CrudRepository<ExchangeRate, Integer> {

    Optional<ExchangeRate> findByCurrency(Currency currency);

}
