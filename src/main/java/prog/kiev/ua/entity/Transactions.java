package prog.kiev.ua.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "transactions")
@Data
public class Transactions {
    @Id
    private Long id;
    private Date time;

    @ManyToOne
    @JoinColumn(name = "sourse_id", referencedColumnName = "id")
    private BankAccount source;

    @ManyToOne
    @JoinColumn(name = "destination_id", referencedColumnName = "id")
    private BankAccount destination;
    private Long amount;
}
