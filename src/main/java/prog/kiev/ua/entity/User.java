package prog.kiev.ua.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "users")
@Data
public class User {
    @Id
    private Integer id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST},  mappedBy = "user")
    List<BankAccount> bankAccounts;
}
