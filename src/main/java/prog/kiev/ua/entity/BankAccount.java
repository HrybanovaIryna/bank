package prog.kiev.ua.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "bank_account")
@Data
public class BankAccount {
    @Id
    private Long id;
    private long amount;
    private Currency currency;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bank_account_id", nullable = false)
    private User user;
}
