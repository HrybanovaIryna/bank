package prog.kiev.ua.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "exchangeRate")
@Data
public class ExchangeRate {
    @Id
    @GeneratedValue
    private Integer id;
    @Enumerated
    private Currency currency;
    private long rate;
}

