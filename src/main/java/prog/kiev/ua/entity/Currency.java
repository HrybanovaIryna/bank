package prog.kiev.ua.entity;

public enum Currency {
    USD,
    EUR,
    UAH
}