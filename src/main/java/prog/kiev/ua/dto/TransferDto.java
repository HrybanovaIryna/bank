package prog.kiev.ua.dto;

import lombok.Data;

@Data
public class TransferDto {
    private long sourceId;
    private long destinationId;
    private long amount;
}
