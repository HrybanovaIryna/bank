package prog.kiev.ua.dto;

import lombok.Data;
import prog.kiev.ua.entity.Currency;

@Data
public class BankAccountDto {
    private Long accountId;
    private long amount;
    private Currency currency;
}
