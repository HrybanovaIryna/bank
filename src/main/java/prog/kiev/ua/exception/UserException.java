package prog.kiev.ua.exception;

public class UserException extends RuntimeException {

    public UserException(String message) {
        super(message);
    }
}
