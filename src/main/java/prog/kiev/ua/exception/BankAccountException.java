package prog.kiev.ua.exception;

public class BankAccountException extends RuntimeException {

    public BankAccountException(String message) {
        super(message);
    }
}
