package prog.kiev.ua.exception;

public class CurrencyException extends RuntimeException{
    public CurrencyException(String message) {
        super(message);
    }
}
