package prog.kiev.ua.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import prog.kiev.ua.dto.BankAccountDto;
import prog.kiev.ua.dto.TransferDto;
import prog.kiev.ua.entity.BankAccount;
import prog.kiev.ua.service.BankAccountService;
import prog.kiev.ua.service.TransactionService;
import prog.kiev.ua.service.UserService;

@RestController
@RequestMapping("/bank")
@RequiredArgsConstructor
public class BankController {
    private final TransactionService transactionService;
    private final BankAccountService bankAccountService;
    private final UserService userService;

    @PostMapping("/add")
    public boolean addFunds(@RequestBody BankAccountDto bankAccountDto) {
        try {
            BankAccount destination = bankAccountService.getAccount(bankAccountDto.getAccountId());
            transactionService.addFunds(destination, bankAccountDto.getAmount(), bankAccountDto.getCurrency());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @PutMapping("/transfer")
    public boolean transferFunds(@RequestBody TransferDto transferDto) {
        try {
            BankAccount source = bankAccountService.getAccount(transferDto.getSourceId());
            BankAccount destination = bankAccountService.getAccount(transferDto.getDestinationId());
            transactionService.transferFunds(source, destination, transferDto.getAmount());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
