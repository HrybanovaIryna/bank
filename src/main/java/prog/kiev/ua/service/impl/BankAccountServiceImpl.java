package prog.kiev.ua.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import prog.kiev.ua.entity.BankAccount;
import prog.kiev.ua.exception.BankAccountException;
import prog.kiev.ua.repository.BankAccountRepository;
import prog.kiev.ua.service.BankAccountService;

@Service
@RequiredArgsConstructor
public class BankAccountServiceImpl implements BankAccountService {

    private final BankAccountRepository bankAccountRepository;

    @Override
    public void saveAccount(BankAccount account) {
        bankAccountRepository.save(account);
    }

    @Override
    public BankAccount getAccount(Long accountId) {
        return bankAccountRepository.findById(accountId)
                .orElseThrow(() -> new BankAccountException("Bank account not found. Id: " + accountId)) ;
    }
}
