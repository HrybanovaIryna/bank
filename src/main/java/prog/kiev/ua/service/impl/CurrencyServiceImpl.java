package prog.kiev.ua.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import prog.kiev.ua.entity.Currency;
import prog.kiev.ua.entity.ExchangeRate;
import prog.kiev.ua.exception.CurrencyException;
import prog.kiev.ua.repository.RateRepository;
import prog.kiev.ua.service.CurrencyService;

@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final RateRepository rateRepository;

    @Override
    public long convertCurrency(Currency from, Currency to, long amount) {
        ExchangeRate exchangeRateFrom = rateRepository.findByCurrency(from)
                .orElseThrow(() -> new CurrencyException("Currency not found " + from));
        ExchangeRate exchangeRateTo = rateRepository.findByCurrency(to)
                .orElseThrow(() -> new CurrencyException("Currency not found " + to));
        return amount * (exchangeRateFrom.getRate() / exchangeRateTo.getRate());
    }
}
