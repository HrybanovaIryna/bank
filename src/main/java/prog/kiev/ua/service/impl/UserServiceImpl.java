package prog.kiev.ua.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import prog.kiev.ua.entity.User;
import prog.kiev.ua.exception.UserException;
import prog.kiev.ua.repository.UserRepository;
import prog.kiev.ua.service.UserService;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public User getUser(Integer userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new UserException("user not found. ID: " + userId));
    }
}
