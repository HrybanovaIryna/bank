package prog.kiev.ua.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import prog.kiev.ua.entity.BankAccount;
import prog.kiev.ua.entity.Currency;
import prog.kiev.ua.entity.User;
import prog.kiev.ua.exception.BankAccountException;
import prog.kiev.ua.service.BankAccountService;
import prog.kiev.ua.service.CurrencyService;
import prog.kiev.ua.service.TransactionService;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final BankAccountService bankAccountService;
    private final CurrencyService currencyService;

    @Transactional
    @Override
    public void addFunds(BankAccount account, long amount, Currency currency) {
        long funds = currencyService.convertCurrency(currency, account.getCurrency(), amount);
        account.setAmount(account.getAmount() + funds);
        bankAccountService.saveAccount(account);
    }

    @Transactional
    @Override
    public void transferFunds(BankAccount source, BankAccount destination, long amount) {
        if (source.getAmount() < amount) {
            throw new BankAccountException("Not enough funds. BankAccount id: " + source.getId()
                    + ", required amount: " + amount + ", available funds " + source.getAmount());
        }
        long funds = currencyService.convertCurrency(source.getCurrency(), destination.getCurrency(), amount);
        destination.setAmount(destination.getAmount() + funds);
        bankAccountService.saveAccount(destination);
        source.setAmount(source.getAmount() - amount);
        bankAccountService.saveAccount(source);
    }

    @Override
    public void currencyExchange(User user, Currency from, Currency to, long amount) {
        BankAccount fromAccount = getBankAccount(user, from);
        BankAccount toAccount = getBankAccount(user, to);
        transferFunds(fromAccount, toAccount, amount);
    }

    private BankAccount getBankAccount(User user, Currency currency) {
        return user.getBankAccounts().stream()
                .filter(bankAccount -> bankAccount.getCurrency() == currency)
                .findFirst()
                .orElseThrow(() -> new BankAccountException("User with id: " + user.getId() + " doesn't have account " + currency));
    }

    @Override
    public Long getAllFundsUAH(User user) {
        return user.getBankAccounts().stream()
                .map(account -> currencyService.convertCurrency(account.getCurrency(), Currency.UAH, account.getAmount()))
                .reduce((amount1, amount2) -> amount1 + amount2)
                .orElse(0L);
    }

}
