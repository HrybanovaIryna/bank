package prog.kiev.ua.service;

import prog.kiev.ua.entity.User;

public interface UserService {

    User getUser(Integer id);
}
