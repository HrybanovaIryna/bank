package prog.kiev.ua.service;

import prog.kiev.ua.entity.BankAccount;
import prog.kiev.ua.entity.Currency;
import prog.kiev.ua.entity.User;

public interface TransactionService {
    void addFunds(BankAccount account, long amount, Currency currency);
    void transferFunds(BankAccount source, BankAccount destination, long amount);
    void currencyExchange(User user, Currency from, Currency to, long amount);
    Long getAllFundsUAH(User user);
}
