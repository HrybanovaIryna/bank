package prog.kiev.ua.service;

import prog.kiev.ua.entity.BankAccount;

public interface BankAccountService {

    void saveAccount(BankAccount account);
    BankAccount getAccount(Long accountId);
}
