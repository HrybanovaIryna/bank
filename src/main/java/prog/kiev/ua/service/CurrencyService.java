package prog.kiev.ua.service;

import prog.kiev.ua.entity.Currency;

public interface CurrencyService {

    long convertCurrency(Currency from, Currency to, long amount);
}